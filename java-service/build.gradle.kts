import com.google.protobuf.gradle.*

plugins {
    id("org.springframework.boot") version "2.3.5.RELEASE"
    id("io.spring.dependency-management") version "1.0.10.RELEASE"

    id("com.google.protobuf") version "0.8.13"

    id("idea")
    id("application")

    java

    id("com.bmuschko.docker-spring-boot-application") version "6.6.0"
}

group = "ru.mrwilde.egar"
version = "0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_11

repositories {
    mavenCentral()
}

docker {
    springBootApplication {
        baseImage.set("adoptopenjdk/openjdk11:ubi")
        maintainer.set("Stanislav Migunov 'stanislav.migunov@outlook.com'")
        ports.set(listOf(9090, 9090))
        jvmArgs.set(listOf("-Xmx2048m"))
    }
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter")
    testImplementation("org.springframework.boot:spring-boot-starter-test") {
        exclude("org.junit.vintage", "junit-vintage-engine")
    }

    implementation("net.devh:grpc-server-spring-boot-starter:2.10.1.RELEASE")
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("org.springframework.boot:spring-boot-starter-jdbc")

    //implementation("org.postgresql:postgresql")
    implementation("com.h2database:h2:1.4.200")

    implementation("com.google.protobuf:protobuf-java:3.13.0")
    implementation("com.google.protobuf:protobuf-java-util:3.13.0")
    implementation("io.grpc:grpc-protobuf:1.33.1")
}

tasks.withType<Test> {
    useJUnitPlatform()
}

application {
    mainClassName = "ru.mrwilde.egar.webservice.WebserviceApplication"
}

protobuf {
    generatedFilesBaseDir = "$projectDir/generatedProtobuf"

    protoc { artifact = "com.google.protobuf:protoc:3.13.0" }

    plugins {
        id("grpc-java") {
            artifact = "io.grpc:protoc-gen-grpc-java:1.33.1"
        }
    }
    generateProtoTasks {
        all().forEach {
            it.plugins {
                id("grpc-java") { }
            }
        }
    }
}

sourceSets {
    main {
        proto {
            srcDir("src/main/protobuf")
        }
    }
}

tasks {
    "clean"(Delete::class) {
        delete("$projectDir/generatedProtobuf")
    }
}
