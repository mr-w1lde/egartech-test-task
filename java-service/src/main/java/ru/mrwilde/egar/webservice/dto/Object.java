package ru.mrwilde.egar.webservice.dto;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name = "objects")
public class Object {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    private Floater floater;

    @Column(name = "price")
    private Integer price;

    @Column(name = "date")
    private Instant date;

    public Object() {

    }

    public Object(Long id, Floater floater, Integer price, Instant date) {
        this.id = id;
        this.floater = floater;
        this.price = price;
        this.date = date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Floater getFloater() {
        return floater;
    }

    public void setFloater(Floater floater) {
        this.floater = floater;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Instant getDate() {
        return date;
    }

    public void setDate(Instant date) {
        this.date = date;
    }
}
