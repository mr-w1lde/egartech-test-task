package ru.mrwilde.egar.webservice.grpc;

import com.google.protobuf.Empty;
import com.google.protobuf.Int64Value;
import com.google.protobuf.util.Timestamps;
import io.grpc.StatusException;
import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.server.service.GrpcService;
import org.springframework.beans.factory.annotation.Autowired;
import ru.mrwilde.egar.api.CommonGrpc;
import ru.mrwilde.egar.api.UpdateFloaterQuery;
import ru.mrwilde.egar.api.UpdateObjectQuery;
import ru.mrwilde.egar.common.*;
import ru.mrwilde.egar.webservice.dto.Floater;
import ru.mrwilde.egar.webservice.dto.Object;
import ru.mrwilde.egar.webservice.services.FloaterService;
import ru.mrwilde.egar.webservice.services.ObjectService;

import java.time.Instant;

@GrpcService
public class EgarServiceImpl extends CommonGrpc.CommonImplBase {
    @Autowired
    private FloaterService floaterService;

    @Autowired
    private ObjectService objectService;

    @Override
    public void addObject(MutableObject request, StreamObserver<ImmutableObject> responseObserver) {
        try {
            Floater floater = new Floater(
                    request.getFloater().getId(),
                    request.getFloater().getTitle()
            );

            Object saved = objectService.saveObject(
                    floater,
                    request.getPrice(),
                    Instant.ofEpochMilli(Timestamps.toMillis(request.getDate()))
            );

            responseObserver.onNext(objectService.toImmutable(saved));
            responseObserver.onCompleted();
        } catch (StatusException e) {
            responseObserver.onError(e);
        }
    }

    @Override
    public void updateObject(UpdateObjectQuery request, StreamObserver<ImmutableObject> responseObserver) {
        try {
            Floater floater = new Floater();
            floater.setId(request.getFloaterId());

            Object newObj = new Object();
            newObj.setFloater(floater);
            newObj.setPrice(request.getPrice());
            newObj.setDate(Instant.ofEpochMilli(Timestamps.toMillis(request.getDate())));

            Object updatable = objectService.updateById(request.getId(), newObj);

            responseObserver.onNext(objectService.toImmutable(updatable));
            responseObserver.onCompleted();
        } catch (StatusException e) {
            responseObserver.onError(e);
        }
    }


    @Override
    public void removeObject(Int64Value request, StreamObserver<Empty> responseObserver) {
        try {
            objectService.deleteById(request.getValue());

            responseObserver.onNext(Empty.getDefaultInstance());
            responseObserver.onCompleted();
        } catch (StatusException e) {
            responseObserver.onError(e);
        }
    }

    @Override
    public void addFloater(MutableFloater request, StreamObserver<ImmutableFloater> responseObserver) {
        try {
            Floater savedFloater = floaterService.saveFloater(request.getTitle());

            responseObserver.onNext(floaterService.toImmutable(savedFloater));
            responseObserver.onCompleted();
        } catch (StatusException e) {
            responseObserver.onError(e);
        }
    }

    @Override
    public void updateFloater(UpdateFloaterQuery request, StreamObserver<ImmutableFloater> responseObserver) {
        try {
            Floater floater = new Floater();
            floater.setTitle(request.getFloater().getTitle());

            Floater updatable = floaterService.updateById(request.getId(), floater);

            responseObserver.onNext(floaterService.toImmutable(updatable));
            responseObserver.onCompleted();
        } catch (StatusException e) {
            responseObserver.onError(e);
        }
    }


    @Override
    public void removeFloater(Int64Value request, StreamObserver<Empty> responseObserver) {
        try {
            floaterService.deleteById(request.getValue());

            responseObserver.onNext(Empty.getDefaultInstance());
            responseObserver.onCompleted();
        } catch (StatusException e) {
            responseObserver.onError(e);
        }
    }

    @Override
    public void getAllFloaters(Empty request, StreamObserver<ImmutableFloater> responseObserver) {
        try {
            floaterService.getAll().forEach( (floater) -> {
                ImmutableFloater temp = floaterService.toImmutable(floater);
                responseObserver.onNext(temp);
            });

            responseObserver.onCompleted();
        } catch (StatusException e) {
            responseObserver.onError(e);
        }
    }

    @Override
    public void getAllObjects(Empty request, StreamObserver<ImmutableObject> responseObserver) {
        try {
            objectService.getAll().forEach( (obj) -> {
                ImmutableObject temp = objectService.toImmutable(obj);
                responseObserver.onNext(temp);
            });

            responseObserver.onCompleted();
        } catch (StatusException e) {
            responseObserver.onError(e);
        }
    }

    @Override
    public void getObjectsByFloaterId(Int64Value request, StreamObserver<ImmutableObject> responseObserver) {
        try {
            objectService.getAllByFloaterId(request.getValue()).forEach( (obj) -> {
                responseObserver.onNext(objectService.toImmutable(obj));
            });

            responseObserver.onCompleted();
        } catch (StatusException e) {
            responseObserver.onError(e);
        }
    }

}
