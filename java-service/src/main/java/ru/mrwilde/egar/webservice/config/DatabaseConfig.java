package ru.mrwilde.egar.webservice.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

@Configuration
public class DatabaseConfig {

    @Autowired
    private Environment environment;

    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();

        dataSource.setDriverClassName("org.h2.Driver");
        dataSource.setUsername("sa");
        dataSource.setPassword("");

        String host = environment.getProperty("DB_HOST");
        String port = environment.getProperty("DB_PORT");
        String database = environment.getProperty("DB_DATABASE");

        String url = String.format("jdbc:h2:tcp://%s:%s/%s", host, port, database);

        dataSource.setUrl(url);

        return dataSource;
    }
}
