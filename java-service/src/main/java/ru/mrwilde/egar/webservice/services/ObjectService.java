package ru.mrwilde.egar.webservice.services;

import com.google.protobuf.util.Timestamps;
import io.grpc.Status;
import io.grpc.StatusException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.mrwilde.egar.common.ImmutableFloater;
import ru.mrwilde.egar.common.ImmutableObject;
import ru.mrwilde.egar.webservice.dto.Floater;
import ru.mrwilde.egar.webservice.dto.Object;
import ru.mrwilde.egar.webservice.repos.ObjectRepo;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Service
public class ObjectService {
    private final Logger logger = LoggerFactory.getLogger(ObjectService.class);

    @Autowired
    private ObjectRepo objectRepo;

    @Autowired
    private FloaterService floaterService;

    public Object saveObject(Floater floater, Integer price, Instant date) throws StatusException {
        try {
            Object object = new Object();

            object.setFloater(floater);
            object.setPrice(price);
            object.setDate(date);

            return objectRepo.save(object);
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw Status.INTERNAL.withDescription(e.getLocalizedMessage()).asException();
        }
    }

    public Iterable<Object> getAll() throws StatusException {
        try {
            return objectRepo.findAll();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw Status.INTERNAL.withDescription(e.getLocalizedMessage()).asException();
        }
    }

    public List<Object> getAllByFloaterId(long id) throws StatusException {
        try {
            return objectRepo.findAllByFloaterId(id);
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw Status.INTERNAL.withDescription(e.getLocalizedMessage()).asException();
        }
    }

    public void deleteById(long id) throws StatusException {
        try {
            objectRepo.deleteById(id);
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw Status.INTERNAL.withDescription(e.getLocalizedMessage()).asException();
        }
    }

    public Object updateById(long id, Object newObject) throws StatusException {
        try {
            Optional<Object> findObject = objectRepo.findById(id);
            if(findObject.isEmpty()) {
                throw Status.NOT_FOUND.withDescription("Can't update object with id " + id + ". Not found").asException();
            }

            newObject.setId(id);

            return objectRepo.save(newObject);
        } catch (StatusException e) {
            throw new StatusException(e.getStatus());
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw Status.INTERNAL.withDescription(e.getLocalizedMessage()).asException();
        }
    }

    public ImmutableObject toImmutable(Object object) {
        ImmutableFloater floater = floaterService.toImmutable(object.getFloater());

        return ImmutableObject.newBuilder()
                .setId(object.getId())
                .setFloater(floater)
                .setPrice(object.getPrice())
                .setDate(Timestamps.fromMillis(object.getDate().toEpochMilli()))
                .build();
    }
}
