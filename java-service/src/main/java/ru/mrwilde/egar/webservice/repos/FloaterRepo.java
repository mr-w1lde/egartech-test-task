package ru.mrwilde.egar.webservice.repos;

import org.springframework.data.repository.CrudRepository;
import ru.mrwilde.egar.webservice.dto.Floater;

public interface FloaterRepo extends CrudRepository<Floater, Long> {
}
