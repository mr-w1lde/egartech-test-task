package ru.mrwilde.egar.webservice.services;

import io.grpc.Status;
import io.grpc.StatusException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.mrwilde.egar.common.ImmutableFloater;
import ru.mrwilde.egar.webservice.dto.Floater;
import ru.mrwilde.egar.webservice.dto.Object;
import ru.mrwilde.egar.webservice.repos.FloaterRepo;

import java.sql.SQLException;
import java.util.Optional;

@Service
public class FloaterService {
    private final Logger logger = LoggerFactory.getLogger(FloaterService.class);

    @Autowired
    private FloaterRepo floaterRepo;

    public Floater saveFloater(String title) throws StatusException {
        try {
            Floater floater = new Floater();
            floater.setTitle(title);

            return floaterRepo.save(floater);
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw Status.INTERNAL.withDescription(e.getLocalizedMessage()).asException();
        }
    }

    public Iterable<Floater> getAll() throws StatusException {
        try {
            return floaterRepo.findAll();
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw Status.INTERNAL.withDescription(e.getLocalizedMessage()).asException();
        }
    }

    public void deleteById(long id) throws StatusException {
        try {
            floaterRepo.deleteById(id);
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw Status.INTERNAL.withDescription(e.getLocalizedMessage()).asException();
        }
    }

    public Floater updateById(long id, Floater newObject) throws StatusException {
        try {
            Optional<Floater> findObject = floaterRepo.findById(id);
            if(findObject.isEmpty()) {
                throw Status.NOT_FOUND.withDescription("Can't update floater with id " + id + ". Not found").asException();
            }

            newObject.setId(id);

            return floaterRepo.save(newObject);
        } catch (StatusException e) {
            throw new StatusException(e.getStatus());
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw Status.INTERNAL.withDescription(e.getLocalizedMessage()).asException();
        }
    }

    public ImmutableFloater toImmutable(Floater floater) {
        return ImmutableFloater.newBuilder()
                .setId(floater.getId())
                .setTitle(floater.getTitle())
                .build();
    }
}
