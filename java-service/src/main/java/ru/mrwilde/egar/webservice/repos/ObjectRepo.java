package ru.mrwilde.egar.webservice.repos;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import ru.mrwilde.egar.webservice.dto.Object;

import java.util.List;

public interface ObjectRepo extends CrudRepository<Object, Long> {
    @Query(value = "SELECT * FROM OBJECTS WHERE FLOATER_ID = ?1 ORDER BY date ASC", nativeQuery = true)
    List<Object> findAllByFloaterId(Long floaterId);
}