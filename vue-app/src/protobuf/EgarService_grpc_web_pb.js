/**
 * @fileoverview gRPC-Web generated client stub for Egar.API
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!


/* eslint-disable */
// @ts-nocheck



const grpc = {};
grpc.web = require('grpc-web');


var Object_pb = require('./Object_pb.js')

var Floater_pb = require('./Floater_pb.js')

var google_protobuf_empty_pb = require('google-protobuf/google/protobuf/empty_pb.js')

var google_protobuf_wrappers_pb = require('google-protobuf/google/protobuf/wrappers_pb.js')

var google_protobuf_timestamp_pb = require('google-protobuf/google/protobuf/timestamp_pb.js')
const proto = {};
proto.Egar = {};
proto.Egar.API = require('./EgarService_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.Egar.API.CommonClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.Egar.API.CommonPromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.Egar.Common.MutableObject,
 *   !proto.Egar.Common.ImmutableObject>}
 */
const methodDescriptor_Common_AddObject = new grpc.web.MethodDescriptor(
  '/Egar.API.Common/AddObject',
  grpc.web.MethodType.UNARY,
  Object_pb.MutableObject,
  Object_pb.ImmutableObject,
  /**
   * @param {!proto.Egar.Common.MutableObject} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  Object_pb.ImmutableObject.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.Egar.Common.MutableObject,
 *   !proto.Egar.Common.ImmutableObject>}
 */
const methodInfo_Common_AddObject = new grpc.web.AbstractClientBase.MethodInfo(
  Object_pb.ImmutableObject,
  /**
   * @param {!proto.Egar.Common.MutableObject} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  Object_pb.ImmutableObject.deserializeBinary
);


/**
 * @param {!proto.Egar.Common.MutableObject} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.Egar.Common.ImmutableObject)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.Egar.Common.ImmutableObject>|undefined}
 *     The XHR Node Readable Stream
 */
proto.Egar.API.CommonClient.prototype.addObject =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/Egar.API.Common/AddObject',
      request,
      metadata || {},
      methodDescriptor_Common_AddObject,
      callback);
};


/**
 * @param {!proto.Egar.Common.MutableObject} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.Egar.Common.ImmutableObject>}
 *     Promise that resolves to the response
 */
proto.Egar.API.CommonPromiseClient.prototype.addObject =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/Egar.API.Common/AddObject',
      request,
      metadata || {},
      methodDescriptor_Common_AddObject);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.Egar.API.UpdateObjectQuery,
 *   !proto.Egar.Common.ImmutableObject>}
 */
const methodDescriptor_Common_UpdateObject = new grpc.web.MethodDescriptor(
  '/Egar.API.Common/UpdateObject',
  grpc.web.MethodType.UNARY,
  proto.Egar.API.UpdateObjectQuery,
  Object_pb.ImmutableObject,
  /**
   * @param {!proto.Egar.API.UpdateObjectQuery} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  Object_pb.ImmutableObject.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.Egar.API.UpdateObjectQuery,
 *   !proto.Egar.Common.ImmutableObject>}
 */
const methodInfo_Common_UpdateObject = new grpc.web.AbstractClientBase.MethodInfo(
  Object_pb.ImmutableObject,
  /**
   * @param {!proto.Egar.API.UpdateObjectQuery} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  Object_pb.ImmutableObject.deserializeBinary
);


/**
 * @param {!proto.Egar.API.UpdateObjectQuery} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.Egar.Common.ImmutableObject)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.Egar.Common.ImmutableObject>|undefined}
 *     The XHR Node Readable Stream
 */
proto.Egar.API.CommonClient.prototype.updateObject =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/Egar.API.Common/UpdateObject',
      request,
      metadata || {},
      methodDescriptor_Common_UpdateObject,
      callback);
};


/**
 * @param {!proto.Egar.API.UpdateObjectQuery} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.Egar.Common.ImmutableObject>}
 *     Promise that resolves to the response
 */
proto.Egar.API.CommonPromiseClient.prototype.updateObject =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/Egar.API.Common/UpdateObject',
      request,
      metadata || {},
      methodDescriptor_Common_UpdateObject);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.google.protobuf.Int64Value,
 *   !proto.google.protobuf.Empty>}
 */
const methodDescriptor_Common_RemoveObject = new grpc.web.MethodDescriptor(
  '/Egar.API.Common/RemoveObject',
  grpc.web.MethodType.UNARY,
  google_protobuf_wrappers_pb.Int64Value,
  google_protobuf_empty_pb.Empty,
  /**
   * @param {!proto.google.protobuf.Int64Value} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  google_protobuf_empty_pb.Empty.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.google.protobuf.Int64Value,
 *   !proto.google.protobuf.Empty>}
 */
const methodInfo_Common_RemoveObject = new grpc.web.AbstractClientBase.MethodInfo(
  google_protobuf_empty_pb.Empty,
  /**
   * @param {!proto.google.protobuf.Int64Value} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  google_protobuf_empty_pb.Empty.deserializeBinary
);


/**
 * @param {!proto.google.protobuf.Int64Value} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.google.protobuf.Empty)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.google.protobuf.Empty>|undefined}
 *     The XHR Node Readable Stream
 */
proto.Egar.API.CommonClient.prototype.removeObject =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/Egar.API.Common/RemoveObject',
      request,
      metadata || {},
      methodDescriptor_Common_RemoveObject,
      callback);
};


/**
 * @param {!proto.google.protobuf.Int64Value} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.google.protobuf.Empty>}
 *     Promise that resolves to the response
 */
proto.Egar.API.CommonPromiseClient.prototype.removeObject =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/Egar.API.Common/RemoveObject',
      request,
      metadata || {},
      methodDescriptor_Common_RemoveObject);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.Egar.Common.MutableFloater,
 *   !proto.Egar.Common.ImmutableFloater>}
 */
const methodDescriptor_Common_AddFloater = new grpc.web.MethodDescriptor(
  '/Egar.API.Common/AddFloater',
  grpc.web.MethodType.UNARY,
  Floater_pb.MutableFloater,
  Floater_pb.ImmutableFloater,
  /**
   * @param {!proto.Egar.Common.MutableFloater} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  Floater_pb.ImmutableFloater.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.Egar.Common.MutableFloater,
 *   !proto.Egar.Common.ImmutableFloater>}
 */
const methodInfo_Common_AddFloater = new grpc.web.AbstractClientBase.MethodInfo(
  Floater_pb.ImmutableFloater,
  /**
   * @param {!proto.Egar.Common.MutableFloater} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  Floater_pb.ImmutableFloater.deserializeBinary
);


/**
 * @param {!proto.Egar.Common.MutableFloater} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.Egar.Common.ImmutableFloater)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.Egar.Common.ImmutableFloater>|undefined}
 *     The XHR Node Readable Stream
 */
proto.Egar.API.CommonClient.prototype.addFloater =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/Egar.API.Common/AddFloater',
      request,
      metadata || {},
      methodDescriptor_Common_AddFloater,
      callback);
};


/**
 * @param {!proto.Egar.Common.MutableFloater} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.Egar.Common.ImmutableFloater>}
 *     Promise that resolves to the response
 */
proto.Egar.API.CommonPromiseClient.prototype.addFloater =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/Egar.API.Common/AddFloater',
      request,
      metadata || {},
      methodDescriptor_Common_AddFloater);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.Egar.API.UpdateFloaterQuery,
 *   !proto.Egar.Common.ImmutableFloater>}
 */
const methodDescriptor_Common_UpdateFloater = new grpc.web.MethodDescriptor(
  '/Egar.API.Common/UpdateFloater',
  grpc.web.MethodType.UNARY,
  proto.Egar.API.UpdateFloaterQuery,
  Floater_pb.ImmutableFloater,
  /**
   * @param {!proto.Egar.API.UpdateFloaterQuery} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  Floater_pb.ImmutableFloater.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.Egar.API.UpdateFloaterQuery,
 *   !proto.Egar.Common.ImmutableFloater>}
 */
const methodInfo_Common_UpdateFloater = new grpc.web.AbstractClientBase.MethodInfo(
  Floater_pb.ImmutableFloater,
  /**
   * @param {!proto.Egar.API.UpdateFloaterQuery} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  Floater_pb.ImmutableFloater.deserializeBinary
);


/**
 * @param {!proto.Egar.API.UpdateFloaterQuery} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.Egar.Common.ImmutableFloater)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.Egar.Common.ImmutableFloater>|undefined}
 *     The XHR Node Readable Stream
 */
proto.Egar.API.CommonClient.prototype.updateFloater =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/Egar.API.Common/UpdateFloater',
      request,
      metadata || {},
      methodDescriptor_Common_UpdateFloater,
      callback);
};


/**
 * @param {!proto.Egar.API.UpdateFloaterQuery} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.Egar.Common.ImmutableFloater>}
 *     Promise that resolves to the response
 */
proto.Egar.API.CommonPromiseClient.prototype.updateFloater =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/Egar.API.Common/UpdateFloater',
      request,
      metadata || {},
      methodDescriptor_Common_UpdateFloater);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.google.protobuf.Int64Value,
 *   !proto.google.protobuf.Empty>}
 */
const methodDescriptor_Common_RemoveFloater = new grpc.web.MethodDescriptor(
  '/Egar.API.Common/RemoveFloater',
  grpc.web.MethodType.UNARY,
  google_protobuf_wrappers_pb.Int64Value,
  google_protobuf_empty_pb.Empty,
  /**
   * @param {!proto.google.protobuf.Int64Value} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  google_protobuf_empty_pb.Empty.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.google.protobuf.Int64Value,
 *   !proto.google.protobuf.Empty>}
 */
const methodInfo_Common_RemoveFloater = new grpc.web.AbstractClientBase.MethodInfo(
  google_protobuf_empty_pb.Empty,
  /**
   * @param {!proto.google.protobuf.Int64Value} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  google_protobuf_empty_pb.Empty.deserializeBinary
);


/**
 * @param {!proto.google.protobuf.Int64Value} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.google.protobuf.Empty)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.google.protobuf.Empty>|undefined}
 *     The XHR Node Readable Stream
 */
proto.Egar.API.CommonClient.prototype.removeFloater =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/Egar.API.Common/RemoveFloater',
      request,
      metadata || {},
      methodDescriptor_Common_RemoveFloater,
      callback);
};


/**
 * @param {!proto.google.protobuf.Int64Value} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.google.protobuf.Empty>}
 *     Promise that resolves to the response
 */
proto.Egar.API.CommonPromiseClient.prototype.removeFloater =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/Egar.API.Common/RemoveFloater',
      request,
      metadata || {},
      methodDescriptor_Common_RemoveFloater);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.google.protobuf.Empty,
 *   !proto.Egar.Common.ImmutableFloater>}
 */
const methodDescriptor_Common_GetAllFloaters = new grpc.web.MethodDescriptor(
  '/Egar.API.Common/GetAllFloaters',
  grpc.web.MethodType.SERVER_STREAMING,
  google_protobuf_empty_pb.Empty,
  Floater_pb.ImmutableFloater,
  /**
   * @param {!proto.google.protobuf.Empty} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  Floater_pb.ImmutableFloater.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.google.protobuf.Empty,
 *   !proto.Egar.Common.ImmutableFloater>}
 */
const methodInfo_Common_GetAllFloaters = new grpc.web.AbstractClientBase.MethodInfo(
  Floater_pb.ImmutableFloater,
  /**
   * @param {!proto.google.protobuf.Empty} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  Floater_pb.ImmutableFloater.deserializeBinary
);


/**
 * @param {!proto.google.protobuf.Empty} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.Egar.Common.ImmutableFloater>}
 *     The XHR Node Readable Stream
 */
proto.Egar.API.CommonClient.prototype.getAllFloaters =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/Egar.API.Common/GetAllFloaters',
      request,
      metadata || {},
      methodDescriptor_Common_GetAllFloaters);
};


/**
 * @param {!proto.google.protobuf.Empty} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.Egar.Common.ImmutableFloater>}
 *     The XHR Node Readable Stream
 */
proto.Egar.API.CommonPromiseClient.prototype.getAllFloaters =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/Egar.API.Common/GetAllFloaters',
      request,
      metadata || {},
      methodDescriptor_Common_GetAllFloaters);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.google.protobuf.Empty,
 *   !proto.Egar.Common.ImmutableObject>}
 */
const methodDescriptor_Common_GetAllObjects = new grpc.web.MethodDescriptor(
  '/Egar.API.Common/GetAllObjects',
  grpc.web.MethodType.SERVER_STREAMING,
  google_protobuf_empty_pb.Empty,
  Object_pb.ImmutableObject,
  /**
   * @param {!proto.google.protobuf.Empty} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  Object_pb.ImmutableObject.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.google.protobuf.Empty,
 *   !proto.Egar.Common.ImmutableObject>}
 */
const methodInfo_Common_GetAllObjects = new grpc.web.AbstractClientBase.MethodInfo(
  Object_pb.ImmutableObject,
  /**
   * @param {!proto.google.protobuf.Empty} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  Object_pb.ImmutableObject.deserializeBinary
);


/**
 * @param {!proto.google.protobuf.Empty} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.Egar.Common.ImmutableObject>}
 *     The XHR Node Readable Stream
 */
proto.Egar.API.CommonClient.prototype.getAllObjects =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/Egar.API.Common/GetAllObjects',
      request,
      metadata || {},
      methodDescriptor_Common_GetAllObjects);
};


/**
 * @param {!proto.google.protobuf.Empty} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.Egar.Common.ImmutableObject>}
 *     The XHR Node Readable Stream
 */
proto.Egar.API.CommonPromiseClient.prototype.getAllObjects =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/Egar.API.Common/GetAllObjects',
      request,
      metadata || {},
      methodDescriptor_Common_GetAllObjects);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.google.protobuf.Int64Value,
 *   !proto.Egar.Common.ImmutableObject>}
 */
const methodDescriptor_Common_GetObjectsByFloaterId = new grpc.web.MethodDescriptor(
  '/Egar.API.Common/GetObjectsByFloaterId',
  grpc.web.MethodType.SERVER_STREAMING,
  google_protobuf_wrappers_pb.Int64Value,
  Object_pb.ImmutableObject,
  /**
   * @param {!proto.google.protobuf.Int64Value} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  Object_pb.ImmutableObject.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.google.protobuf.Int64Value,
 *   !proto.Egar.Common.ImmutableObject>}
 */
const methodInfo_Common_GetObjectsByFloaterId = new grpc.web.AbstractClientBase.MethodInfo(
  Object_pb.ImmutableObject,
  /**
   * @param {!proto.google.protobuf.Int64Value} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  Object_pb.ImmutableObject.deserializeBinary
);


/**
 * @param {!proto.google.protobuf.Int64Value} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.Egar.Common.ImmutableObject>}
 *     The XHR Node Readable Stream
 */
proto.Egar.API.CommonClient.prototype.getObjectsByFloaterId =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/Egar.API.Common/GetObjectsByFloaterId',
      request,
      metadata || {},
      methodDescriptor_Common_GetObjectsByFloaterId);
};


/**
 * @param {!proto.google.protobuf.Int64Value} request The request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.Egar.Common.ImmutableObject>}
 *     The XHR Node Readable Stream
 */
proto.Egar.API.CommonPromiseClient.prototype.getObjectsByFloaterId =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/Egar.API.Common/GetObjectsByFloaterId',
      request,
      metadata || {},
      methodDescriptor_Common_GetObjectsByFloaterId);
};


module.exports = proto.Egar.API;

