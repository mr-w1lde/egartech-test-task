![Иллюстрация к проекту](https://i.ibb.co/BCPX0Jz/Screenshot-2020-11-10-at-00-49-37.png)
# Тестовое задание 
    - Описание: 
        Простой CRUD Сервис, который позволяет добавлять, изменять, удалять и просматривать данные (акции), 
        а также следить за статистикой изменений акций через график
    - Запуск: 
        Для запуска всех необходимых ресурсов используем Docker, 
        $ docker-compose up -d 
    - Ресурсы: 
        * http://localhost:80 - Vue приложение
        * localhost:9090 - gRPC API ( для тестирования можно использовать [evans](https://github.com/ktr0731/evans) )
        * http://localhost:8081 - Envoy Proxy для gRPC-Web 
        * http://localhost:81: - Web панель H2 базы данных
    - Project Stack:
        * Java 
        * VueJS
        * H2
        * Envoy Proxy
        * gRPC + gRPC-Web
        * Docker
